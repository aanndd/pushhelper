//
//  UserDefaultManager.swift
//  PushHelper
//
//  Created by Anand Yadav on 12/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit

struct UserDefaultManager {
    static let tokenKey = "pusher.deviceToken"

    static var deviceToken: String? {
        get {
            return UserDefaults.standard.object(forKey: tokenKey) as? String
        }
        set {
            if newValue != nil {
                UserDefaults.standard.set(newValue, forKey: tokenKey)
            } else {
                UserDefaults.standard.removeObject(forKey: tokenKey)
            }
        }
    }
}
