//
//  PushNotificationHelper.swift
//  PushHelper
//
//  Created by Anand Yadav on 12/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit
import UserNotifications

let _sharedPushHelper: PushNotificationHelper = { PushNotificationHelper() }()

class PushNotificationHelper {
    
    class func shared() -> PushNotificationHelper {
        return _sharedPushHelper
    }
    
    //To register remote notification
    func registerForRemoteNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            
            if error != nil {
                print("Notifications permission denied because: \(String(describing: error?.localizedDescription)).")

            } else {
                DispatchQueue.main.async {
                    print("Notifications permission granted.")
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
//            if granted {
//                DispatchQueue.main.async {
//                    print("Notifications permission granted.")
//                    UIApplication.shared.registerForRemoteNotifications()
//                }
//            } else {
//                print("Notifications permission denied because: \(String(describing: error?.localizedDescription)).")
//            }
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        UserDefaultManager.deviceToken = deviceTokenString
        print("APNs device token: \(deviceTokenString)")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("APNs registration failed: \(error)")
    }
    
    
    //    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
    //        switch application.applicationState {
    //
    //        case .inactive:
    //            print("Inactive")
    //            //Show the view with the content of the push
    //            completionHandler(.newData)
    //
    //        case .background:
    //            print("Background")
    //            //Refresh the local model
    //            completionHandler(.newData)
    //
    //        case .active:
    //            print("Active")
    //            //Show an in-app banner
    //            completionHandler(.newData)
    //        }
    //    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print(userInfo)
        switch application.applicationState {
            
        case .inactive:
            print("Inactive")
            //Show the view with the content of the push
            completionHandler(.newData)
            
        case .background:
            print("Background")
            //Refresh the local model
            completionHandler(.newData)
            
        case .active:
            print("Active")
            //Show an in-app banner
            completionHandler(.newData)
        default:
            break
        }
    }
}
