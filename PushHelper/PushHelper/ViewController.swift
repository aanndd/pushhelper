//
//  ViewController.swift
//  PushHelper
//
//  Created by Anand Yadav on 09/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnGetNotificationClicked(_ sender: Any) {
        // Request Notification Settings
//        UNUserNotificationCenter.current().getNotificationSettings { (notificationSettings) in
//            switch notificationSettings.authorizationStatus {
//            case .notDetermined:
//                self.requestAuthorization(completionHandler: { (success) in
//                    guard success else { return }
//                    
//                    // Schedule Local Notification
//                    self.scheduleLocalNotification()
//                })
//            case .authorized:
//                // Schedule Local Notification
//                self.scheduleLocalNotification()
//            case .denied:
//                print("Application Not Allowed to Display Notifications")
//            case .provisional:
//                print("Application Not Allowed to Display Notifications")
//            @unknown default:
//                print("Application Not Allowed to Display Notifications")
//            }
//        }
    }
    
//    private func requestAuthorization(completionHandler: @escaping (_ success: Bool) -> ()) {
//        // Request Authorization
//        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (success, error) in
//            if let error = error {
//                print("Request Authorization Failed (\(error), \(error.localizedDescription))")
//            }
//
//            completionHandler(success)
//        }
//    }
    
//    private func scheduleLocalNotification() {
//        // Create Notification Content
//        let notificationContent = UNMutableNotificationContent()
//
//        // Configure Notification Content
//        notificationContent.title = "Cocoacasts"
//        notificationContent.subtitle = "Local Notifications"
//        notificationContent.body = "In this tutorial, you learn how to schedule local notifications with the User Notifications framework."
//
//        // Add Trigger
//        let notificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 10.0, repeats: false)
//
//        // Create Notification Request
//        let notificationRequest = UNNotificationRequest(identifier: "cocoacasts_local_notification", content: notificationContent, trigger: notificationTrigger)
//
//        // Add Request to User Notification Center
//        UNUserNotificationCenter.current().add(notificationRequest) { (error) in
//            if let error = error {
//                print("Unable to Add Notification Request (\(error), \(error.localizedDescription))")
//            }
//        }
//    }
    
}

//extension ViewController: UNUserNotificationCenterDelegate {
//
//    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//        completionHandler([.alert])
//    }
//
//}
